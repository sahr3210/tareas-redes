# Revisar fallos en pipeline

## Estados del pipeline

Como se mencionó en el [documento del flujo de trabajo](README.md).
El _pipeline_ es una rutina que permite ejecutar un proceso automatizado cada vez que se hace _push_ a una rama del repositorio principal o de algún _fork_.

Esto hace que todas las tareas que se entregan en el [repositorio central de tareas][repositorio-tareas] puedan ser visualizadas en la vista web del [sitio estatico][repositorio-vista-web].

Después de la ejecución del _pipeline_, se pueden tener dos estados:

- <span style="color: green; font-weight: bold;">passed</span>: No existen problemas en el material entregado.

- <span style="color: red; font-weight: bold;">failed</span>: Existen fallas que no permiten que se pueda construir el sitio estático con los últimos cambios versionados.

En el caso de que el estado del _pipeline_ de CI/CD sea `failed`, se tendrán que realizar las siguientes verificaciones:

1. Los nombres de archivo no tienen espacios, acentos o caracteres especiales:
    - Se recomienda que se separen los elementos del nombre del archivo con guiones medios.
    - Ej. `archivo-con-un-nombre-largo.md` o `foto-del-plano-de-red.jpg`.

2. Los hipervínculos que se incluyen en los archivos _markdown_ hacen referencia a archivos válidos:
    - El archivo al que se hace referencia **existe** y no está listado en el archivo `.gitignore`.
    - El archivo **no contiene** espacios, acentos o caracteres especiales en el nombre.
    - El archivo **esta en la carpeta adecuada**.
        - Las imágenes van dentro del directorio `img`.
        - Los archivos adicionales van dentro del directorio `files`.

3. El formato del archivo Markdown es correcto:

  Así se escribe una liga hacia otro documento o archivo:

```
[archivo README.md](ruta/hacia/README.md)
[Escenario de Packet Tracer](files/archivo.pkt)
[Captura de tráfico de WireShark](files/captura.pcap)
```

  Así se escribe una liga hacia una imagen:

```
![](img/imagen.png)
```

## Probar los cambios localmente

### Instalar requisitos

Instalar los requisitos para construir el sitio de manera local

```
$ sudo -i
	...
# apt install build-essential make python3-pip python3-dev
```

### Ejecutar mkdocs

Se ejecuta `mkdocs` de manera local para visualizar errores, `mkdocs` se instala de manera local si es necesario:

```
$ make
which mkdocs || \
pip3 install --user --requirement requirements.txt
/home/tonejito/.local/bin/mkdocs
mkdocs serve --strict --verbose
INFO    -  Building documentation...
INFO    -  Cleaning site directory
WARNING -  Documentation file 'workflow/README.md' contains a link to 'workflow/img/000-workflow.png' which is not found in the documentation files.

Exited with 1 warnings in strict mode.
make: *** [Makefile:17: serve] Error 1
```

En este caso el mensaje de error muestra que la imagen `workflow/img/000-workflow.png` no existe y esta siendo referenciada en el archivo `workflow/README.md`.

Ejecutar `make` después de resolver los conflictos

```
$ make
which mkdocs || \
pip3 install --user --requirement requirements.txt
/home/tonejito/.local/bin/mkdocs
mkdocs serve --strict --verbose
INFO    -  Building documentation...
INFO    -  Cleaning site directory
INFO    -  Documentation built in 0.31 seconds
[I 210310 01:02:03 server:335] Serving on http://127.0.0.1:8000
INFO    -  Serving on http://127.0.0.1:8000
[I 210310 01:02:03 handlers:62] Start watching changes
INFO    -  Start watching changes
[I 210310 01:02:03 handlers:64] Start detecting changes
INFO    -  Start detecting changes

	...
```

Si los conflictos fueron resueltos, el sitio será visible en <http://localhost:8000/>

## Revisar pipelines en la interfaz de GitLab

### Revisar lista de pipelines

Para revisar los fallos de pipeline en la interfaz de GitLab, ir a la sección de **CI/CD** y luego a [**Pipelines**][repositorio-tareas]

| [Repositorio de tareas][repositorio-tareas] |
|:-------------------------------------------:|
| ![](img/debug-001-CI-CD_pipelines.png)      |

### Revisar pipeline fallido

Busca en la lista de _pipelines_ para ver cual fue el que falló

| [Lista de pipelines][repositorio-pipelines] |
|:-------------------------------------------:|
| ![](img/debug-002-pipeline_list.png)        |

Da clic en el pipeline [`#265156407`][pipeline-failed]

| [_Pipeline_ fallído][pipeline-failed]  |
|:--------------------------------------:|
| ![](img/debug-003-pipeline_failed.png) |

Da clic en `test` para abrir el estado del trabajo [`#1071780438`][job-failed]

| [_Job_ fallído][job-failed]       |
|:---------------------------------:|
| ![](img/debug-004-job_failed.png) |

- Aquí se muestran los pasos que se siguieron para probar la construcción del sitio estático
- Ignora las líneas que tienen el prefijo `DEBUG`
- Las líneas `106` a `115` indican que el problema es que se hace referencia a archivos que no existen en el repositorio:

```
106  WARNING -  Documentation file 'workflow/README.md' contains a link to 'workflow/img/001-Main_repo.png' which is not found in the documentation files.

	...

115  WARNING -  Documentation file 'workflow/README.md' contains a link to 'workflow/img/009-Main_MR_merged.png' which is not found in the documentation files.
```

### Resolver problemas

Hacer los cambios correspondientes para resolver el problema

- Se agregan los archivos faltantes al repositorio, se hace `commit` y `push`.

```
$ git add docs/workflow/img/???-*.png
$ git commit -m "Agregando imagenes faltantes"
$ git push
```

### Revisar si el nuevo pipeline fue exitoso

Después de corregir los cambios y enviarlos al repositorio, se abre de nuevo la [página de los _pipelines_ de CI/CD][repositorio-pipelines] para verificar que ya no haya problemas.

- Como resultado ya no hay conflictos en el [nuevo _pipeline_ `#265158713`][pipeline-passed] que se ejecutó

| [_Pipeline_ exitoso][pipeline-passed]  |
|:--------------------------------------:|
| ![](img/debug-005-pipeline_passed.png) |

- Adicionalmente ya no hay errores en el [nuevo _job_ `#1071791158`][job-passed] que se ejecutó.

| [_Job_ exitoso][job-passed]       |
|:---------------------------------:|
| ![](img/debug-006-job_passed.png) |

Con esto se resolvieron los problemas del _pipeline_ en el [repositorio de tareas][repositorio-pipelines].

>>>
**Notas**:

- El _merge request_ se actualiza automáticamente cuando se hace push al repositorio _fork_.
- Debes revisar la sección de _pipelines_ de CI/CD para **tu repositorio**, puesto que ahi es donde se listan los errores que puedas tener.
>>>

--------------------------------------------------------------------------------

[repositorio-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes.git
[repositorio-vista-web]: https://redes-ciencias-unam.gitlab.io/2021-2/tareas-redes/
[repositorio-pipelines]: https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes/-/pipelines
[pipeline-failed]: https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes/-/pipelines/265156407
[job-failed]: https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes/-/jobs/1071780438
[pipeline-passed]: https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes/-/pipelines/265158713
[job-passed]: https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes/-/jobs/1071791158
